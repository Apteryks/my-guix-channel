* Personal GNU Guix channel

This channel provides experimental packages that aren't yet ready for
being upstreamed to GNU Guix.

To use it, add the following channel configuration to your
=~/.config/guix/channels.scm= file:

#+BEGIN_SRC scheme
  (cons (channel
	 (name 'my-guix-channel)
	 (url "https://gitlab.com/Apteryks/my-guix-channel"))
	%default-channels)
#+END_SRC

For more information regarding Guix channels, refer to "info
'(guix)Channels'"
([[https://guix.gnu.org/manual/en/html_node/Channels.html]]).
